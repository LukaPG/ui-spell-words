var difficulty = 1;
var bigletters = false;
var current_src = "";
var current_name = "";
var correct_letters = 0;

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.firstChild.firstChild.innerHTML);
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    console.log(data, ev.target.id);
    if(data == ev.target.id || data == ev.target.parentNode.id){
        ev.target.firstChild.innerHTML = data;
        var audio = new Audio('../media/success.mp3');
        correct_letters++;
    }else{
        M.toast({html: 'Napacna crka!'});
        var audio = new Audio('../media/fail.mp3');
    }
    audio.play();
    if(correct_letters == current_name.length){
        M.toast({html: 'Bravo!'});
        var audio = new Audio('../media/success.mp3');
        audio.play();
    }
}


function setImage(src, name){
    if(bigletters == true){
        name = name.toUpperCase();
    }else{
        name = name.toLowerCase();
    }
    $("#image").attr("src", src);
    current_name = name;
    current_src = src;
    var namelen = name.length;
    $("#template").parent().html(`<p>Povleci crke na njihova mesta:</p><div id="template"></div>`);

    switch(difficulty){
        case 1:
            $("#template").html("");
            for (var i=0;i<namelen;i++){
                $("#template").append(`<div class='card target blue col s${Math.floor(12/namelen)}' style="width:${80/namelen}%; padding-top:${100/namelen}px;"><div id="${name[i]}" ondrop='drop(event)' ondragover='allowDrop(event)' class='card-content'><h4>${name[i]}</h4></div></div>`);
            }
        break;
        case 2:
            $("#template").html("");
            $("#template").parent().html(`<p>Povleci crke na njihova mesta:</p><p><i>Beseda je ${name}</i></p><div id="template"></div>`)
            for (var i=0;i<namelen;i++){
                $("#template").append(`<div class='card target blue col s${Math.floor(12/namelen)}' style="width:${80/namelen}%; padding-top:${100/namelen}px;"><div id="${name[i]}" ondrop='drop(event)' ondragover='allowDrop(event)' class='card-content'><h4></h4></div></div>`);
            }
        break;
        case 3:
            $("#template").html("");
            for (var i=0;i<namelen;i++){
                $("#template").append(`<div class='card target blue col s${Math.floor(12/namelen)}' style="width:${80/namelen}%; padding-top:${100/namelen}px;"><div id="${name[i]}" ondrop='drop(event)' ondragover='allowDrop(event)' class='card-content'><h4></h4></div></div>`);
            }
        break;
    }
    
    

    var letters = name.split("");
    shuffle(letters);
    $("#letters").html("");
    for (var i=0;i<letters.length;i++){
        $("#letters").append(`<div class='col s${Math.floor(12/letters.length)} card blue' ondragstart="drag(event)" draggable="true"><div class='card-content center-align'><h3>${letters[i]}</h3></div></div>`);
    }
    correct_letters=0;
}


function setDif(level){
    difficulty = level;
    setImage(current_src, current_name);
}

function setLetters(){
    let cbox = document.getElementById("lett");
    bigletters = cbox.checked;
    setImage(current_src, current_name);
}

var bckgaudio = new Audio("../media/music.mp3");
bckgaudio.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);

function setMusic(){
    let cbox = document.getElementById("music");
    
    if(cbox.checked == true){
        bckgaudio.play(); 
    }else{
        bckgaudio.pause();
    }
}


function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

$(document).ready(function(){

    //inits
    var elem = document.getElementById('set');
    var instances = M.Dropdown.init(elem, {hover:true, coverTrigger:false, constrainWidth:false, closeOnClick:false});

    elem = document.getElementById('ims');
    instances = M.Dropdown.init(elem, {hover:true, coverTrigger:false, constrainWidth:true});

    var words;
    $.getJSON("../words.json", function(data){
        //console.log(data);
        words = data["slike"];

        var num = getRndInteger(0, words.length);
        var source = words[num]["slika"];
        var text = words[num]["tekst"];
        current_name = text;
        current_src = source;
        setUl();
        setImage(source, text);
        
    });

    function setUl(){
        for (var obj in words){
            var src = words[obj]["slika"];
            var name = words[obj]["tekst"];
            var li = `<li><a onclick='setImage("${src}", "${name}")'><img class="responsive-img" src="${src}"></a></li>`;
            $("#imgs").append(li);
        }
    }


    
});